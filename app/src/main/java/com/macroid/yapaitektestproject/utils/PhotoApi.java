package com.macroid.yapaitektestproject.utils;

import com.macroid.yapaitektestproject.response.PhotoResponse;
import com.macroid.yapaitektestproject.response.PhotoSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PhotoApi
{
    //search for photos
    //https://www.flickr.com/services/rest/?
    //method=flickr.photos.search&api_key=e5de63e83f6d27ab8b4b582edf14e0a9&text=mazandaran&format=json&nojsoncallback=1
    @GET("services/rest/?method=flickr.photos.search")
    Call<PhotoResponse> searchPhotos(
            @Query("api_key") String api_key,
            @Query("text") String text,
            @Query("format") String format,
            @Query("nojsoncallback") String nojsoncallback
    );

}
