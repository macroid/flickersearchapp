package com.macroid.yapaitektestproject.request;
import com.macroid.yapaitektestproject.utils.Credentials;
import com.macroid.yapaitektestproject.utils.PhotoApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Service
{

    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(Credentials.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static PhotoApi photoApi = retrofit.create(PhotoApi.class);

    public static PhotoApi getPhotoApi()
    {
        return photoApi;
    }

}
