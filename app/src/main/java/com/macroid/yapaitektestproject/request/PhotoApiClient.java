package com.macroid.yapaitektestproject.request;


import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.macroid.yapaitektestproject.AppExecutors;
import com.macroid.yapaitektestproject.models.PhotoModel;
import com.macroid.yapaitektestproject.response.PhotoResponse;
import com.macroid.yapaitektestproject.utils.Credentials;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

public class PhotoApiClient
{

    //Live Data
    private MutableLiveData<List<PhotoModel>> mPhotos = new MutableLiveData<>();
    private static PhotoApiClient instance;

    //making Global RUNNABLE
    private RetrievePhotosRunnable retrievePhotosRunnable;

    public static PhotoApiClient getInstance()
    {
        if (instance == null)
        {
            instance = new PhotoApiClient();
        }
        return instance;
    }

    private PhotoApiClient()
    {
        mPhotos = new MutableLiveData<>();
    }

    public LiveData<List<PhotoModel>> getPhotos()
    {
        return mPhotos;
    }

    //1-This method call through the classes
    public void searchPhotosApi(String text, String format, String nojsoncallback)
    {
        if (retrievePhotosRunnable != null)
        {
            retrievePhotosRunnable = null;
        }
        retrievePhotosRunnable = new RetrievePhotosRunnable(text, format, nojsoncallback);

        final Future mHandler = AppExecutors.getInstance().NetworkIo().submit(retrievePhotosRunnable);

        AppExecutors.getInstance().NetworkIo().schedule(new Runnable()
        {
            @Override
            public void run()
            {
                //Cancelling the retrofit call
                mHandler.cancel(true);
            }
        }, 7000, TimeUnit.MILLISECONDS);

    }

    //Retriving data from RESTAPI by runnable class
    private class RetrievePhotosRunnable implements Runnable
    {

        private String text;
        private String format;
        private String nojsoncallback;
        boolean cancelRequest;

        public RetrievePhotosRunnable(String text, String format, String nojsoncallback)
        {
            this.text = text;
            this.format = format;
            this.nojsoncallback = nojsoncallback;
            cancelRequest = false;
        }

        @Override
        public void run()
        {
            //Getting the response objects
            try
            {
                Response<PhotoResponse> response = getPhotos(text, format, nojsoncallback).execute();
                if (cancelRequest)
                {
                    return;
                }
                if (response.code() == 200)
                {

                    assert response.body() != null;
                    List<PhotoModel> photoModelList = new ArrayList<>(response.body().getResponseModel().getPhotoModel());
                    //Sending data to Live data
                    //PostValue: used for background thread
                    mPhotos.postValue(photoModelList);
                } else
                {
                    //String error = response.errorBody().string();
                    //Log.v("Tag", "Error " + error);
                    mPhotos.postValue(null);
                }
            } catch (Exception e)
            {
                e.printStackTrace();
                mPhotos.postValue(null);
            }


        }

        //Search method and query
        private Call<PhotoResponse> getPhotos(String text, String format, String
                nojsoncallback)
        {
            return Service.getPhotoApi().searchPhotos(
                    Credentials.API_KEY,
                    text,
                    format,
                    nojsoncallback
            );
        }

        private void cancelRequest()
        {
            Log.v("Tag", "Cancelling Search Request");
            cancelRequest = true;
        }
    }




}
