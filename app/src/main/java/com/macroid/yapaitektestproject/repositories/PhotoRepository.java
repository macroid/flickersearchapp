package com.macroid.yapaitektestproject.repositories;

import androidx.lifecycle.LiveData;

import com.macroid.yapaitektestproject.models.PhotoModel;
import com.macroid.yapaitektestproject.request.PhotoApiClient;

import java.util.List;

public class PhotoRepository
{
    //This class is acting as repositories
    private static PhotoRepository instance;
    private PhotoApiClient photoApiClient;

    public static PhotoRepository getInstance()
    {
        if (instance == null)
        {
            instance = new PhotoRepository();
        }
        return instance;
    }

    private PhotoRepository()
    {
        photoApiClient = PhotoApiClient.getInstance();
    }

    public LiveData<List<PhotoModel>> getPhotos()
    {
        return photoApiClient.getPhotos();
    }

    //2-Calling the method in repository
    public void searchPhotoApi(String text, String format, String nojsoncallback)
    {
        photoApiClient.searchPhotosApi(text, format, nojsoncallback);
    }

}


