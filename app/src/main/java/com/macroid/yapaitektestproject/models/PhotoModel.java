package com.macroid.yapaitektestproject.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoModel implements Parcelable
{
    //modelclass for photos
    @SerializedName("id")
    @Expose
    private String photoId;
    @SerializedName("owner")
    @Expose
    private String photoOwner;
    @SerializedName("secret")
    @Expose
    private String photoSecret;
    @SerializedName("server")
    @Expose
    private String photoServerId;
    @SerializedName("farm")
    @Expose
    private int photoFarm;
    @SerializedName("title")
    @Expose
    private String photoTitle;
    @SerializedName("ispublic")
    @Expose
    private int photoIspublic;
    @SerializedName("isfriend")
    @Expose
    private int photoIsFriend;
    @SerializedName("isfamily")
    @Expose
    private int photoIsFamily;



    //constractor

    public PhotoModel(String photoId, String photoOwner, String photoSecret, String photoServerId, int photoFarm, String photoTitle, int photoIspublic, int photoIsFriend, int photoIsFamily)
    {
        this.photoId = photoId;
        this.photoOwner = photoOwner;
        this.photoSecret = photoSecret;
        this.photoServerId = photoServerId;
        this.photoFarm = photoFarm;
        this.photoTitle = photoTitle;
        this.photoIspublic = photoIspublic;
        this.photoIsFriend = photoIsFriend;
        this.photoIsFamily = photoIsFamily;
    }


    //getter

    protected PhotoModel(Parcel in)
    {
        photoId = in.readString();
        photoOwner = in.readString();
        photoSecret = in.readString();
        photoServerId = in.readString();
        photoFarm = in.readInt();
        photoTitle = in.readString();
        photoIspublic = in.readInt();
        photoIsFriend = in.readInt();
        photoIsFamily = in.readInt();
    }

    public static final Creator<PhotoModel> CREATOR = new Creator<PhotoModel>()
    {
        @Override
        public PhotoModel createFromParcel(Parcel in)
        {
            return new PhotoModel(in);
        }

        @Override
        public PhotoModel[] newArray(int size)
        {
            return new PhotoModel[size];
        }
    };

    public String getPhotoId()
    {
        return photoId;
    }

    public void setPhotoId(String photoId)
    {
        this.photoId = photoId;
    }

    public String getPhotoOwner()
    {
        return photoOwner;
    }

    public void setPhotoOwner(String photoOwner)
    {
        this.photoOwner = photoOwner;
    }

    public String getPhotoSecret()
    {
        return photoSecret;
    }

    public void setPhotoSecret(String photoSecret)
    {
        this.photoSecret = photoSecret;
    }

    public String getPhotoServerId()
    {
        return photoServerId;
    }

    public void setPhotoServerId(String photoServerId)
    {
        this.photoServerId = photoServerId;
    }

    public int getPhotoFarm()
    {
        return photoFarm;
    }

    public void setPhotoFarm(int photoFarm)
    {
        this.photoFarm = photoFarm;
    }

    public String getPhotoTitle()
    {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle)
    {
        this.photoTitle = photoTitle;
    }

    public int getPhotoIspublic()
    {
        return photoIspublic;
    }

    public void setPhotoIspublic(int photoIspublic)
    {
        this.photoIspublic = photoIspublic;
    }

    public int getPhotoIsFriend()
    {
        return photoIsFriend;
    }

    public void setPhotoIsFriend(int photoIsFriend)
    {
        this.photoIsFriend = photoIsFriend;
    }

    public int getPhotoIsFamily()
    {
        return photoIsFamily;
    }

    public void setPhotoIsFamily(int photoIsFamily)
    {
        this.photoIsFamily = photoIsFamily;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(photoId);
        parcel.writeString(photoOwner);
        parcel.writeString(photoSecret);
        parcel.writeString(photoServerId);
        parcel.writeInt(photoFarm);
        parcel.writeString(photoTitle);
        parcel.writeInt(photoIspublic);
        parcel.writeInt(photoIsFriend);
        parcel.writeInt(photoIsFamily);
    }

    public String generaitPath()
    {
        return photoServerId+"/"+photoId+"_"+photoSecret+"_"+"q.jpg";
    }

}
