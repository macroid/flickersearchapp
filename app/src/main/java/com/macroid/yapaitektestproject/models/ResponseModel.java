package com.macroid.yapaitektestproject.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel
{

    private int page;
    private int pages;
    private int perpage;
    private String total;
    @SerializedName("photo")
    private List<PhotoModel> photoModel;


    //constractor
    public ResponseModel(int page, int pages, int perpage, String total, List<PhotoModel> photoModel)
    {
        this.page = page;
        this.pages = pages;
        this.perpage = perpage;
        this.total = total;
        this.photoModel = photoModel;
    }


    //getter

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getPages()
    {
        return pages;
    }

    public void setPages(int pages)
    {
        this.pages = pages;
    }

    public int getPerpage()
    {
        return perpage;
    }

    public void setPerpage(int perpage)
    {
        this.perpage = perpage;
    }

    public String getTotal()
    {
        return total;
    }

    public void setTotal(String total)
    {
        this.total = total;
    }

    public List<PhotoModel> getPhotoModel()
    {
        return photoModel;
    }

    public void setPhotoModel(List<PhotoModel> photoModel)
    {
        this.photoModel = photoModel;
    }
}
