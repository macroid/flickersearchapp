package com.macroid.yapaitektestproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.macroid.yapaitektestproject.R;
import com.macroid.yapaitektestproject.models.PhotoModel;
import com.macroid.yapaitektestproject.utils.Credentials;

import java.util.ArrayList;
import java.util.List;

public class PhotoRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<PhotoModel> photoModels;
    private OnPhotoListener onPhotoListener;

    public PhotoRecyclerView(OnPhotoListener onPhotoListener)
    {
        this.onPhotoListener = onPhotoListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item,
                parent, false);
        return new PhotoViewHolder(view, onPhotoListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        //ImageView Using Glide Library
        Glide.with(holder.itemView.getContext())
                .load(Credentials.DOWNLOAD_URL + photoModels.get(position).generaitPath())
                .into(((PhotoViewHolder) holder).imageView);
    }

    @Override
    public int getItemCount()
    {
        if (photoModels != null)
        {
            return photoModels.size();
        }
        return 0;
    }

    public void setPhotoModels(List<PhotoModel> photoModels)
    {
        this.photoModels = photoModels;
    }


}
