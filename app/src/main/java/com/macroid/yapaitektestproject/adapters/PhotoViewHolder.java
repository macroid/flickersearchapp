package com.macroid.yapaitektestproject.adapters;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.macroid.yapaitektestproject.R;

public class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    //Widgets
    ImageView imageView;

    //Click Listener
    OnPhotoListener onPhotoListener;

    public PhotoViewHolder(@NonNull View itemView , OnPhotoListener onPhotoListener)
    {
        super(itemView);
        this.onPhotoListener = onPhotoListener;
        imageView = itemView.findViewById(R.id.imageView);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        onPhotoListener.onPhotoClick(getAdapterPosition());
    }
}
