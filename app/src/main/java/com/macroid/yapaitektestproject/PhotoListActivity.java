package com.macroid.yapaitektestproject;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.macroid.yapaitektestproject.adapters.OnPhotoListener;
import com.macroid.yapaitektestproject.adapters.PhotoRecyclerView;
import com.macroid.yapaitektestproject.models.PhotoModel;
import com.macroid.yapaitektestproject.viewmodels.PhotoListViewModel;

import java.util.List;

public class PhotoListActivity extends AppCompatActivity implements OnPhotoListener
{
    //Edittext for search
    EditText editText;
    //RecyclerView
    private RecyclerView recyclerView;
    private PhotoRecyclerView photoRecyclerViewAdapter;
    //ViewModel
    private PhotoListViewModel photoListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        photoListViewModel = new ViewModelProvider(this).get(PhotoListViewModel.class);

        //Calling the observers
        ObservAnyChange();
        //RecyclerView
        ConfigureRecyclerView();
        //SearchView
        SetupSearchView();
    }

    //Observing any data change
    private void ObservAnyChange()
    {
        photoListViewModel.getPhotos().observe(this, new Observer<List<PhotoModel>>()
        {
            @Override
            public void onChanged(List<PhotoModel> photoModels)
            {
                //Observing for any data change
                if (photoModels != null)
                {
                    for (PhotoModel photoModel : photoModels)
                    {
                        //Get the data in log
                        //Log.v("Tag", "onChanged " + photoModel.getPhotoTitle());
                        photoRecyclerViewAdapter.setPhotoModels(photoModels);
                    }
                    photoRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    //5-Intializing recyclerview & adding data to it
    private void ConfigureRecyclerView()
    {
        photoRecyclerViewAdapter = new PhotoRecyclerView(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setAdapter(photoRecyclerViewAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3, LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onPhotoClick(int position)
    {
        Toast.makeText(this, "photo item : " + position, Toast.LENGTH_SHORT).show();
    }

    //Get data from searchview
    private void SetupSearchView()
    {
        final SearchView searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String text)
            {
                photoListViewModel.searchPhotoApi(
                        text,
                        "json",
                        "1");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                return false;
            }
        });
    }

}
