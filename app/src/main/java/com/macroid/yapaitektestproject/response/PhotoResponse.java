package com.macroid.yapaitektestproject.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.macroid.yapaitektestproject.models.PhotoModel;
import com.macroid.yapaitektestproject.models.ResponseModel;

//this class for the single photo request
public class PhotoResponse
{
    //finding the photo search
    @SerializedName("photos")
    @Expose()
    private ResponseModel responseModel;

    public ResponseModel getResponseModel()
    {
        return responseModel;
    }

}
