package com.macroid.yapaitektestproject.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.macroid.yapaitektestproject.models.PhotoModel;

import java.util.List;

//this class is for searching photos by text (photo list)
public class PhotoSearchResponse
{

    @SerializedName("photos.photo")
    @Expose()
    private List<PhotoModel> photoModels;



    public List<PhotoModel> getPhotoModels()
    {
        return photoModels;
    }

    public void setPhotoModels(List<PhotoModel> photoModels)
    {
        this.photoModels = photoModels;
    }

}
