package com.macroid.yapaitektestproject.viewmodels;



import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.macroid.yapaitektestproject.models.PhotoModel;
import com.macroid.yapaitektestproject.repositories.PhotoRepository;

import java.util.List;

public class PhotoListViewModel extends ViewModel
{
    private PhotoRepository photoRepository;

    //Constractor
    public PhotoListViewModel()
    {
        photoRepository = PhotoRepository.getInstance();
    }

    public LiveData<List<PhotoModel>> getPhotos()
    {
        return photoRepository.getPhotos();
    }

    //3-Calling method in view-model
    public void searchPhotoApi(String text, String format, String nojsoncallback)
    {
        photoRepository.searchPhotoApi(text, format, nojsoncallback);
    }
}
